import pytest
import pytest_check as check
def ValidSentenceCheck(s):
    if(s[0].islower()):

        return False
    if(s.count('"')%2 ==1):

        return False
    if(s[-1]!= "."):

        return False
    if(s.index(".")!= len(s)-1):
        return False
    for a in s.split():
        if(a.isdigit()):
            if(int(a) < 13):
                return False
    return True


@pytest.mark.parametrize(('x', 'y'), [("The quick brown fox said “hello Mr lazy dog”.", True), ('The quick brown fox said hello Mr lazy dog.', True), ('One lazy dog is too few, 13 is too many.', True),('One lazy dog is too few, thirteen is too many.', True),('The quick brown fox said "hello Mr. lazy dog".', False), ('the quick brown fox said “hello Mr lazy dog".', False), ('"The quick brown fox said “hello Mr lazy dog."', False), ('One lazy dog is too few, 12 is too many.', False)])
def test_simple_assume(x, y):
    check.is_true(ValidSentenceCheck(x) == y)
